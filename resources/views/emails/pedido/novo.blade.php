<html>
    <body>
        <h1>Pedido Realizado com Sucesso!</h1>
        <p>Cliente: {{$pedido->cliente->nome}}</p>
        <p>Código do Pedido: {{$pedido->id}}</p>
        <p><strong>Pastéis</strong></p>
        <ul>
            @foreach($pedido->pasteis as $pastel)
                <li>{{$pastel->nome}} - R$ {{number_format($pastel->preco, 2, ',', '.')}}</li>
            @endforeach
        </ul>
    </body>
</html>
