# Pastelaria API

API RESTFul para gerenciar os pedidos de uma pastelaria.

## Ambiente
- PHP 7.3
- MySQL 5.7
- Laravel 6.2
- MailHog

## Instalação em ambiente Linux

No diretório do projeto, execute os comandos abaixo:

```bash
- docker-compose up -d
- docker exec -it pastelaria-app cp .env.example .env
- docker exec -it pastelaria-app composer install
- docker exec -it pastelaria-app php artisan key:generate
- docker exec -it pastelaria-app php artisan migrate
- docker exec -it pastelaria-app php artisan db:seed 
```

## Rotas

```
GET|HEAD  | api/clientes          
POST      | api/clientes          
GET|HEAD  | api/clientes/{cliente}
PUT       | api/clientes/{cliente}
DELETE    | api/clientes/{cliente}
GET|HEAD  | api/pasteis           
POST      | api/pasteis           
GET|HEAD  | api/pasteis/{pastel}  
PUT|PATCH | api/pasteis/{pastel}  
DELETE    | api/pasteis/{pastel}  
GET|HEAD  | api/pedidos           
POST      | api/pedidos           
GET|HEAD  | api/pedidos/{pedido}  
PUT|PATCH | api/pedidos/{pedido}  
DELETE    | api/pedidos/{pedido}  
```

## Exemplos

Para facilitar os testes, pode-se utilizar o arquivo Pastelaria.postman_collection.json no software [Postaman](https://www.getpostman.com/).

## Caixa de e-mail

Para acessar a caixa de e-mail do MailHog e visualizar os e-mails dos pedidos, acesse o endereço http://127.0.0.1:8025.

## Testes da aplicação

```bash
docker exec -it pastelaria-app vendor/bin/phpunit
```
