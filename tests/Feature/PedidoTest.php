<?php

namespace Tests\Feature;

use Illuminate\Support\Facades\Artisan;
use Tests\TestCase;

class PedidoTest extends TestCase
{
    public function setUp() : void
    {
        parent::setUp();
        Artisan::call('db:seed');
    }

    public function testListaPedidos()
    {
        $response = $this->get(Route('pedidos.index'));
        $response->assertStatus(200);
    }

    public function criarPedido()
    {
        Artisan::call('db:seed');
        $dataCliente = [
            'nome' => 'Teste',
            'email' => 'teste@admin.com',
            'telefone' => '11971717171',
            'dt_nascimento' => '14/01/1988',
            'endereco' => 'Rua teste, 1000',
            'complemento' => 'Teste',
            'bairro' => 'Jardim Teste',
            'cep' => '07120-030'
        ];
        $responseCreate = $this->post(route('clientes.store'), $dataCliente);
        $dataCreate = json_decode($responseCreate->content());
        $dataPedido = [
            'cliente_id' => $dataCreate->data->id,
            'pasteis' => [1]
        ];

        return $this->post(route('pedidos.store'), $dataPedido);
    }

    public function testCriaPedido()
    {
        $responseCreate = $this->criarPedido();
        $dataCreate = json_decode($responseCreate->content());

        $responseCreate->assertStatus(201);
        $this->assertDatabaseHas('pedidos', ['cliente_id' => $dataCreate->data->cliente_id]);
    }

    public function testAtualizaCliente()
    {
        $responseCreate = $this->criarPedido();
        $dataCreate = json_decode($responseCreate->content());

        $dataPedido = [
            'cliente_id' => $dataCreate->data->id,
            'pasteis' => [2]
        ];

        $responseUpdate = $this->put(Route('pedidos.update', ['pedido' => $dataCreate->data->id]), $dataPedido);
        $responseUpdate->assertStatus(200);
    }

    public function testRemoveCliente()
    {
        $responseCreate = $this->criarPedido();
        $dataCreate = json_decode($responseCreate->content());

        $responseDelete = $this->delete(Route('pedidos.destroy', ['pedido' => $dataCreate->data->id]));
        $responseDelete->assertStatus(200);
    }
}
