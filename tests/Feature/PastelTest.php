<?php

namespace Tests\Feature;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;
use Tests\TestCase;

class PastelTest extends TestCase
{
    public $data;

    public function setUp() : void
    {
        parent::setUp();
        $this->data = [
            'nome' => 'Pastel',
            'preco' => '5',
            'foto' => UploadedFile::fake()->image('pastel.jpg')
        ];
    }

    public function criarPastel()
    {
        Storage::fake('public');
        return $this->post(route('pasteis.store'), $this->data);
    }

    public function testListaPasteis()
    {
        $response = $this->get(Route('pasteis.index'));
        $response->assertStatus(200);
    }

    public function testCriaPastel()
    {
        $responseCreate = $this->criarPastel();

        $responseCreate->assertStatus(201);
        $this->assertDatabaseHas('pasteis', ['nome' => $this->data['nome']]);
        Storage::disk('public')->assertExists('images/' . $this->data['foto']->hashName());
    }

    public function testBuscaPastel()
    {
        $responseCreate = $this->criarPastel();
        $dataCreate = json_decode($responseCreate->content());

        $responseShow = $this->get(Route('pasteis.show', ['pastel' => $dataCreate->data->id]));
        $dataShow = json_decode($responseShow->content());

        $responseShow->assertStatus(200);
        $this->assertEquals($this->data['nome'], $dataShow->data->nome);
    }

    public function testAtualizaPastel()
    {
        $responseCreate = $this->criarPastel();
        $dataCreate = json_decode($responseCreate->content());

        $data = $this->data;
        $novo_nome = 'Novo nome';
        $data['nome'] = $novo_nome;

        $responseUpdate = $this->put(Route('pasteis.update', ['pastel' => $dataCreate->data->id]), $data);
        $dataUpdate = json_decode($responseUpdate->content());

        $responseUpdate->assertStatus(200);
        $this->assertEquals($novo_nome, $dataUpdate->data->nome);
    }

    public function testRemovePastel()
    {
        $responseCreate = $this->criarPastel();
        $dataCreate = json_decode($responseCreate->content());

        $this->assertDatabaseHas('pasteis', ['id' => $dataCreate->data->id]);

        $responseDelete = $this->delete(Route('pasteis.destroy', ['pastel' => $dataCreate->data->id]));
        $responseDelete->assertStatus(200);
    }
}
