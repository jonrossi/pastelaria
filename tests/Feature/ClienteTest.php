<?php

namespace Tests\Feature;

use Tests\TestCase;

class ClienteTest extends TestCase
{
    public $data;

    public function setUp() : void
    {
        parent::setUp();
        $this->data = [
            'nome' => 'Teste',
            'email' => 'teste@admin.com',
            'telefone' => '11971717171',
            'dt_nascimento' => '14/01/1988',
            'endereco' => 'Rua teste, 1000',
            'complemento' => 'Teste',
            'bairro' => 'Jardim Teste',
            'cep' => '07120-030'
        ];
    }

    public function criarCliente()
    {
        return $this->post(route('clientes.store'), $this->data);
    }

    public function testListaClientes()
    {
        $response = $this->get(Route('pasteis.index'));
        $response->assertStatus(200);
    }

    public function testCriaCliente()
    {
        $response = $this->criarCliente();
        $response->assertStatus(201);
        $this->assertDatabaseHas('clientes', ['nome' => $this->data['nome']]);
    }

    public function testBuscaCliente()
    {
        $responseCreate = $this->criarCliente();
        $dataCreate = json_decode($responseCreate->content());

        $responseShow = $this->get(Route('clientes.show', ['cliente' => $dataCreate->data->id]));
        $dataShow = json_decode($responseShow->content());

        $responseShow->assertStatus(200);
        $this->assertEquals($this->data['nome'], $dataShow->data->nome);
    }

    public function testAtualizaCliente()
    {
        $responseCreate = $this->criarCliente();
        $dataCreate = json_decode($responseCreate->content());

        $data = $this->data;
        $novo_nome = 'Novo nome';
        $data['nome'] = $novo_nome;

        $responseUpdate = $this->put(Route('clientes.update', ['cliente' => $dataCreate->data->id]), $data);
        $dataUpdate = json_decode($responseUpdate->content());

        $responseUpdate->assertStatus(200);
        $this->assertEquals($novo_nome, $dataUpdate->data->nome);
    }

    public function testRemoveCliente()
    {
        $responseCreate = $this->criarCliente();
        $dataCreate = json_decode($responseCreate->content());

        $this->assertDatabaseHas('clientes', ['id' => $dataCreate->data->id]);

        $responseDelete = $this->delete(Route('clientes.destroy', ['cliente' => $dataCreate->data->id]));
        $responseDelete->assertStatus(200);
    }
}
