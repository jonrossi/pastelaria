<?php

use Illuminate\Database\Seeder;

class PastelTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('pasteis')->insert([
            [
                'nome' => 'Pastel de Carne',
                'preco' => '5.50',
                'foto' => 'images/pastel.jpg',
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'nome' => 'Pastel de Queijo',
                'preco' => '5.50',
                'foto' => 'images/pastel.jpg',
                'created_at' => date("Y-m-d H:i:s")
            ],
            [
                'nome' => 'Pastel de Frango',
                'preco' => '6.00',
                'foto' => 'images/pastel.jpg',
                'created_at' => date("Y-m-d H:i:s")
            ]
        ]);
    }
}
