<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Rotas para o Recurso Cliente, definidas manualmente.
Route::group(['prefix' => 'clientes'], function(){
    Route::get('', 'ClienteController@index')->name('clientes.index');
    Route::post('', 'ClienteController@store')->name('clientes.store');
    Route::get('{cliente}', 'ClienteController@show')->name('clientes.show');
    Route::put('{cliente}', 'ClienteController@update')->name('clientes.update');
    Route::delete('{cliente}', 'ClienteController@destroy')->name('clientes.destroy');
});

//Rotas para o recurso Pastel, definida como resource com algumas configuraçoes.
Route::resource('pasteis', 'PastelController', [
    'parameters' => [
        'pasteis' => 'pastel'
    ]
])->except(['create', 'edit']);

//Rotas para o recurso Pedido, definida como resource.
Route::resource('pedidos', 'PedidoController')->except(['create', 'edit']);
