<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;

class Pedido extends Model
{
    use SoftDeletes;

    protected $hidden = ['updated_at', 'deleted_at', 'cliente'];

    protected $fillable = [
        'cliente_id'
    ];

    public function pasteis()
    {
        return $this->belongsToMany(Pastel::class, 'pedido_pastel');
    }

    public function cliente()
    {
        return $this->belongsTo(Cliente::class);
    }

    public function criarPedido(int $cliente_id, array $pasteis) : Model
    {
        DB::beginTransaction();
        $pedido = $this->create(['cliente_id' => $cliente_id]);
        $pedido->pasteis()->sync($pasteis);
        DB::commit();

        return $pedido;
    }

    public function atualizarPedido(int $cliente_id, array $pasteis) : Model
    {
        DB::beginTransaction();
        $this->update(['cliente_id' => $cliente_id]);
        $this->pasteis()->sync($pasteis);
        DB::commit();

        return $this;
    }

    public function removerPedido() : void
    {
        DB::beginTransaction();
        $this->pasteis()->detach();
        $this->delete();
        DB::commit();
    }
}
