<?php

namespace App\Mail;

use App\Pedido;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SendMailPedido extends Mailable
{
    use Queueable, SerializesModels;

    private $pedido;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Pedido $pedido)
    {
        $this->pedido = $pedido;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('pastelaria@pastelaria.com', 'Pastelaria')
            ->subject('Pedido Realizado na Pastelaria')
            ->view('emails.pedido.novo')
            ->with([
                'pedido' => $this->pedido,
            ]);
    }
}
