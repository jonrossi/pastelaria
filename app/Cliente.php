<?php

namespace App;

use DateTime;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Cliente extends Model
{
    use SoftDeletes;

    protected $hidden = ['updated_at', 'deleted_at'];

    protected $fillable = [
        'nome',
        'email',
        'telefone',
        'dt_nascimento',
        'endereco',
        'complemento',
        'bairro',
        'cep'
    ];

    /**
     * Modificando o campo dt_nascimento com Mutator
     */
    public function setDtNascimentoAttribute($value)
    {
        $date = DateTime::createFromFormat('d/m/Y', $value);
        $this->attributes['dt_nascimento'] = $date->format('Y-m-d');
    }

    /**
     * Modificando o campo dt_nascimento com Accessor
     */
    public function getDtNascimentoAttribute($value)
    {
        $date = DateTime::createFromFormat('Y-m-d', $value);
        return $date->format('d/m/Y');
    }
}
