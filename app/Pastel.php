<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Pastel extends Model
{
    use SoftDeletes;

    protected $table = 'pasteis';

    protected $hidden = ['created_at', 'updated_at', 'deleted_at', 'pivot'];

    protected $fillable = [
        'nome',
        'preco',
        'foto'
    ];
}
