<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ClienteRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nome' => 'required',
            'email' => 'required|email|unique:clientes,email',
            'telefone' => 'required',
            'dt_nascimento' => 'required|date_format:d/m/Y',
            'endereco' => 'required',
            'complemento' => 'required',
            'bairro' => 'required',
            'cep' => 'required'
        ];

        if($this->method() == "PUT"){
            $rules['email'] .= "," . \Request::route('cliente');
        }

        return $rules;
    }
}
