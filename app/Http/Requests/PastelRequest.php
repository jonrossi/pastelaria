<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PastelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nome' => 'required|unique:pasteis,nome',
            'preco' => 'required|regex:/^\d*(\.\d{2})?$/',
            'foto' => 'required|mimes:jpeg,png'
        ];

        if($this->method() == "PUT"){
            $rules['nome'] .= "," . \Request::route('pastel');
        }

        return $rules;
    }
}
