<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Http\Requests\ClienteRequest;
use Illuminate\Http\JsonResponse;

class ClienteController extends BaseController
{
    public function __construct(Cliente $model)
    {
        $this->model = $model;
        $this->nomeRecurso = "Cliente";
    }

    /**
     * Store a newly created resource in storage.
     * @param ClienteRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(ClienteRequest $request) : JsonResponse
    {
        try{
            $cliente = $this->model->create($request->all());
            return $this->sendResponse(201, 'Cliente criado com sucesso!', $cliente);
        }catch(\Exception $e){
            return $this->sendError(500,'Erro ao criar cliente.', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     * @param ClienteRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(ClienteRequest $request, int $id) : JsonResponse
    {
        $cliente = $this->model->find($id);
        if(is_null($cliente)){
            return $this->sendResponse(404);
        }

        try{
            $cliente->update($request->all());
            return $this->sendResponse(200, 'Cliente atualizado com sucesso!', $cliente);
        }catch(\Exception $e){
            return $this->sendError(500,'Erro ao atualizar cliente.', $e->getMessage());
        }
    }
}
