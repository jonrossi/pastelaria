<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;

class BaseController extends Controller
{
    protected $model;

    protected $nomeRecurso;

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() : JsonResponse
    {
        $recursos = $this->model->paginate();
        return response()->json($recursos, 200);
    }

    /**
     * Display the specified resource.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $id) : JsonResponse
    {
        $recurso = $this->model->find($id);
        if(is_null($recurso)){
            return $this->sendResponse(204);
        }
        return $this->sendResponse(200, $this->nomeRecurso . ' selecionado com sucesso!', $recurso);
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $id) : JsonResponse
    {
        $recurso = $this->model->find($id);
        if(is_null($recurso)){
            return $this->sendResponse(404);
        }

        try{
            $recurso->delete();
            return $this->sendResponse(200, $this->nomeRecurso . ' removido com sucesso!');
        }catch(\Exception $e){
            return $this->sendError(500,'Erro ao remover ' . $this->nomeRecurso . '.', $e->getMessage());
        }
    }

    /**
     * @param int $code
     * @param null $message
     * @param null $result
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendResponse(int $code = 200, $message = null, $result = null) : JsonResponse
    {
        $response = null;
        if(!empty($message)){
            $response['message'] = $message;
        }
        if(!empty($result)){
            $response['data'] = $result;
        }
        return response()->json($response, $code);
    }

    /**
     * @param int $code
     * @param null $message
     * @param null $errorMessages
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendError(int $code = 404, $message = null, $errorMessages = null) : JsonResponse
    {
        $response = null;
        if(!empty($message)){
            $response['message'] = $message;
        }
        if(!empty($errorMessages)){
            $response['errors'][] = $errorMessages;
        }
        return response()->json($response, $code);
    }
}
