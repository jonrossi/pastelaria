<?php

namespace App\Http\Controllers;

use App\Http\Requests\PastelRequest;
use App\Pastel;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Storage;

class PastelController extends BaseController
{
    public function __construct(Pastel $model)
    {
        $this->model = $model;
        $this->nomeRecurso = "Pastel";
    }

    /**
     * Store a newly created resource in storage.
     * @param PastelRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PastelRequest $request) : JsonResponse
    {
        $data = $request->all();

        try{
            $data['foto'] = $data['foto']->store('images', 'public');
            $pastel = $this->model->create($data);
            return $this->sendResponse(201, 'Pastel criado com sucesso!', $pastel);
        }catch(\Exception $e){
            return $this->sendError(500,'Erro ao criar pastel.', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     * @param PastelRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(PastelRequest $request, int $id) : JsonResponse
    {
        $pastel = $this->model->find($id);
        if(is_null($pastel)){
            return $this->sendResponse(404);
        }

        $data = $request->all();
        if($request->file('foto')){
            $data['foto'] = $data['foto']->store('images', 'public');
        }

        try{
            $pastel->update($data);
            return $this->sendResponse(200, 'Pastel atualizado com sucesso!', $pastel);
        }catch(\Exception $e){
            return $this->sendError(500,'Erro ao atualizar pastel.', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $id) : JsonResponse
    {
        $pastel = $this->model->find($id);
        if(is_null($pastel)){
            return $this->sendResponse(404);
        }

        try{
            Storage::disk('public')->delete($pastel->foto);
            $pastel->delete();
            return $this->sendResponse(200, $this->nomeRecurso . ' removido com sucesso!');
        }catch(\Exception $e){
            return $this->sendError(500,'Erro ao remover ' . $this->nomeRecurso . '.', $e->getMessage());
        }
    }
}
