<?php

namespace App\Http\Controllers;

use App\Cliente;
use App\Mail\SendMailPedido;
use App\Pastel;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\PedidoRequest;
use App\Pedido;
use Illuminate\Support\Facades\Mail;

class PedidoController extends BaseController
{
    public function __construct(Pedido $model)
    {
        $this->model = $model;
        $this->nomeRecurso = "Pedido";
    }

    /**
     * Display a listing of the resource.
     * @return \Illuminate\Http\JsonResponse
     */
    public function index() : JsonResponse
    {
        $pedidos = $this->model->with('pasteis')->paginate();
        return response()->json($pedidos, 200);
    }

    /**
     * Display the specified resource.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $id) : JsonResponse
    {
        $pedido = $this->model->with('pasteis')->find($id);
        if(is_null($pedido)){
            return $this->sendResponse(204);
        }

        return $this->sendResponse(200,  'Pedido selecionado com sucesso.', $pedido);
    }

    /**
     * Store a newly created resource in storage.
     * @param PedidoRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PedidoRequest $request) : JsonResponse
    {
        $cliente = Cliente::find($request->get('cliente_id'));
        if(is_null($cliente)){
            return $this->sendResponse(404, 'Cliente ' . $request->get('cliente_id') . ' não encontrado!');
        }

        foreach($request->get('pasteis') as $id_pastel){
            $pastel = Pastel::find($id_pastel);
            if(is_null($pastel)){
                return $this->sendResponse(404, 'Pastel ' . $id_pastel . ' não encontrado!');
            }
        }

        try{
            $pedido = $this->model->criarPedido($request->get('cliente_id'), $request->get('pasteis'));

            Mail::to($pedido->cliente->email)->send(new SendMailPedido($pedido));

            return $this->sendResponse(201, 'Pedido criado com sucesso!', $pedido);
        }catch(\Exception $e){
            return $this->sendError(500,'Erro ao criar pedido.', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     * @param PedidoRequest $request
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(PedidoRequest $request, int $id) : JsonResponse
    {
        $pedido = $this->model->find($id);
        if(is_null($pedido)){
            return $this->sendResponse(404);
        }

        $cliente = Cliente::find($request->get('cliente_id'));
        if(is_null($cliente)){
            return $this->sendResponse(404, 'Cliente ' . $request->get('cliente_id') . ' não encontrado!');
        }

        foreach($request->get('pasteis') as $id_pastel){
            $pastel = Pastel::find($id_pastel);
            if(is_null($pastel)){
                return $this->sendResponse(404, 'Pastel ' . $id_pastel . ' não encontrado!');
            }
        }

        try{
            $pedido->atualizarPedido($request->get('cliente_id'), $request->get('pasteis'));
            return $this->sendResponse(200, 'Pedido atualizado com sucesso!', $pedido);
        }catch(\Exception $e){
            return $this->sendError(500,'Erro ao atualizar pedido.', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $id) : JsonResponse
    {
        $pedido = $this->model->find($id);
        if(is_null($pedido)){
            return $this->sendResponse(404);
        }

        try{
            $pedido->removerPedido();
            return $this->sendResponse(200, 'Pedido removido com sucesso!');
        }catch(\Exception $e){
            return $this->sendError(500,'Erro ao remover Pedido.', $e->getMessage());
        }
    }
}
